package com.project.mva.taskmanagermvalib.utils;

/**
 * Created by martinInExt on 17/03/2017.
 */

public class Task {

    private String id;
    public void setId(String id) { this.id = id; }
    public String getId() { return this.id; }

    private String title;
    public void setTitle(String title) { this.title = title; }
    public String getTitle() { return this.title; }

    private String description;
    public void setDescription(String description) { this.description = description; }
    public String getDescription() { return this.description; }

    private String createDate;
    public void setCreateDate(String createDate) { this.createDate = createDate; }
    public String getCreateDate() { return this.createDate; }
}
