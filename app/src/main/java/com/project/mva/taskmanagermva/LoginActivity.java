package com.project.mva.taskmanagermva;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.project.mva.taskmanagermva.storage.Storage;
import com.project.mva.taskmanagermva.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class LoginActivity extends Activity {

    private TextView welcomeTextView;
    private TextView nameTextView;
    private Button logoutButton;
    private Button tasksListButton;
    private Button createTasksButton;
    private StorageReference mStorageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mStorageRef = FirebaseStorage.getInstance().getReference();

        logoutButton = (Button) findViewById(R.id.logoutButton);
        tasksListButton = (Button) findViewById(R.id.tasklistButton);
        createTasksButton = (Button) findViewById(R.id.createTaskButton);

        welcomeTextView = (TextView) findViewById(R.id.welcomeTextView);
        nameTextView = (TextView) findViewById(R.id.nameTextView);

        Storage storage = new Storage();
        String welcome = getString(R.string.welcome);
        String signin = getString(R.string.signin);
        welcomeTextView.setText(welcome + " " + storage.getNameValue(LoginActivity.this));
        nameTextView.setText(signin + " " + storage.getEmailValue(LoginActivity.this));

        /**
         *
         * View List of Tasks created by the Logged in Account
         */
        tasksListButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, TaskListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        /**
         *
         * Go to Task Creation Page
         */
        createTasksButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, TasksActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        /**
         *
         * Logout Account then go to Login Page
         */
        logoutButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                Storage storage = new Storage();
                                storage.updateLogoutValue(LoginActivity.this, storage.DEACTIVE);

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage("Logout Account?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });
    }

}

