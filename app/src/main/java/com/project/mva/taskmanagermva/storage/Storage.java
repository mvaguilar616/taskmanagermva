package com.project.mva.taskmanagermva.storage;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by martinInExt on 16/03/2017.
 */

public class Storage extends Activity{

    public static final String PREF_NAME = "TMMVA01";

    public static final String ACTIVE = "activated";
    public static final String DEACTIVE = "deactivated";
    public static final String EMAIL = "TMMVAemail";
    public static final String EMAIL_VALUE = "test@test.com";
    public static final String PWORD = "TMMVApword";
    public static final String PWORD_VALUE = "test123";
    public static final String NAME = "TMMVAname";
    public static final String NAME_VALUE = "Test";
    public static final String LOGOUT = "TMMVAlogout";
    public static final String LOGOUT_VALUE = "deactivated";
    public static final String TASKS = "TASKS";
    public static final String TASKS_VALUE = "0";

    public boolean createSettings(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, Context.MODE_WORLD_READABLE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(EMAIL, EMAIL_VALUE);
        editor.putString(PWORD, PWORD_VALUE);
        editor.putString(NAME, NAME_VALUE);
        return editor.commit();
    }

    public String getLogoutValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, Context.MODE_WORLD_READABLE);
        return settings.getString(LOGOUT, LOGOUT_VALUE);
    }

    public boolean updateLogoutValue(Activity activity, String logoutValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, Context.MODE_WORLD_READABLE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(LOGOUT, logoutValue);

        return editor.commit();
    }

    public String getEmailValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, Context.MODE_WORLD_READABLE);
        return settings.getString(EMAIL, EMAIL_VALUE);
    }

    public boolean updateEmailValue(Activity activity, String emailValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, Context.MODE_WORLD_READABLE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(EMAIL, emailValue);

        return editor.commit();
    }

    public String getTasksValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, Context.MODE_WORLD_READABLE);
        return settings.getString(TASKS, TASKS_VALUE);
    }

    public boolean updateTasksValue(Activity activity, String tasksValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, Context.MODE_WORLD_READABLE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(TASKS, tasksValue);

        return editor.commit();
    }

    public String getPasswordValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, Context.MODE_WORLD_READABLE);
        return settings.getString(PWORD, PWORD_VALUE);
    }

    public boolean updatePasswordValue(Activity activity, String pwordValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, Context.MODE_WORLD_READABLE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PWORD, pwordValue);

        return editor.commit();
    }

    public String getNameValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, Context.MODE_WORLD_READABLE);
        return settings.getString(NAME, NAME_VALUE);
    }

    public boolean updateNameValue(Activity activity, String nameValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, Context.MODE_WORLD_READABLE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(NAME, nameValue);

        return editor.commit();
    }
}

