package com.project.mva.taskmanagermva;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.project.mva.taskmanagermva.storage.Storage;
import com.project.mva.taskmanagermva.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class CreateAccountActivity extends Activity {

    private EditText nameCAccountEditText;
    private EditText emailCAccountEditText;
    private EditText passwordCAccountEditText;
    private Button createNewAccountButton;
    private StorageReference mStorageRef;
    private ProgressDialog progressInd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        mStorageRef = FirebaseStorage.getInstance().getReference();
        nameCAccountEditText = (EditText) findViewById(R.id.nameNewAccountEditText);
        emailCAccountEditText = (EditText) findViewById(R.id.emailNewAccountEditText);
        passwordCAccountEditText = (EditText) findViewById(R.id.passwordNewAccountEditText);
        createNewAccountButton = (Button) findViewById(R.id.createNewAccountButton);

        /**
         *
         * Create New Account using entered Name, Email & Password
         */
        createNewAccountButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (Utilities.isOnline(CreateAccountActivity.this)) {
                    if (nameCAccountEditText.getText().length() > 0 |
                            emailCAccountEditText.getText().length() > 0 |
                            passwordCAccountEditText.getText().length() > 0) {

                        progressInd = new ProgressDialog(v.getContext());
                        progressInd.setCancelable(true);
                        progressInd.setMessage("Creating Account...");
                        progressInd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressInd.setProgress(0);
                        progressInd.setMax(100);
                        progressInd.show();

                        StorageReference riversRef = mStorageRef.child("useraccount/"+emailCAccountEditText.getText()+".txt");
                        try {
                            JSONObject data = Utilities.createAccount(emailCAccountEditText.getText().toString(),
                                    passwordCAccountEditText.getText().toString(),nameCAccountEditText.getText().toString(), Storage.DEACTIVE);

                            String dataAccount = data.toString();
                            InputStream stream;

                            stream = new ByteArrayInputStream(dataAccount.getBytes("UTF-8"));
                            riversRef.putStream(stream).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    nameCAccountEditText.setText("");
                                    emailCAccountEditText.setText("");
                                    passwordCAccountEditText.setText("");
                                    Utilities.displayDialog(CreateAccountActivity.this, "Account Created");
                                    progressInd.dismiss();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Utilities.displayDialog(CreateAccountActivity.this, "Error Creating Account, Data Invalid.");
                                }
                            });
                        } catch (UnsupportedEncodingException uencExp) {
                            Utilities.displayDialog(CreateAccountActivity.this, "Error Creating Account, Data Invalid.");
                        } catch (JSONException jExp) {
                            Utilities.displayDialog(CreateAccountActivity.this, "Error Creating Account, Data Invalid.");
                        }
                    } else {
                        Utilities.displayDialog(CreateAccountActivity.this, "Error Creating Account, Data Invalid.");
                    }
                } else {
                    Utilities.displayDialog(CreateAccountActivity.this, "Failed to connect to server. Please check your internet connection.");
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CreateAccountActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
