package com.project.mva.taskmanagermva;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.view.View;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.storage.*;

import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;

import com.google.android.gms.tasks.OnSuccessListener;
import com.project.mva.taskmanagermva.storage.Storage;
import com.project.mva.taskmanagermva.utils.Utilities;
import com.project.mva.taskmanagermva.utils.Account;

import org.json.JSONException;

import android.content.DialogInterface;
import android.app.AlertDialog;
import android.app.ProgressDialog;


public class MainActivity extends Activity {

    private Button loginButton;
    private EditText emailEditText;
    private EditText passwordEditText;
    private Button createAccountButton;
    private StorageReference mStorageRef;
    private ProgressDialog progressInd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Storage storage = new Storage();
        if (storage.getLogoutValue(MainActivity.this).equals("activated")) {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        mStorageRef = FirebaseStorage.getInstance().getReference();
        loginButton = (Button) findViewById(R.id.loginButton);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        createAccountButton = (Button) findViewById(R.id.createAccountButton);

        /**
         *
         * Login using existing Email and correct Password
         */
        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Utilities.isOnline(MainActivity.this)) {
                    if (emailEditText.getText().length() > 0) {

                        progressInd = new ProgressDialog(v.getContext());
                        progressInd.setCancelable(true);
                        progressInd.setMessage("Logging in...");
                        progressInd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressInd.setProgress(0);
                        progressInd.setMax(100);
                        progressInd.show();

                        StorageReference riversRef = mStorageRef.child("useraccount/"+emailEditText.getText()+".txt");
                        try {
                            final File localFile = File.createTempFile(emailEditText.getText().toString(),"txt");
                            riversRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                                    StringBuilder text = new StringBuilder();

                                    try {
                                        BufferedReader br = new BufferedReader(new FileReader(localFile));
                                        String line;

                                        while ((line = br.readLine()) != null) {
                                            text.append(line);
                                            text.append('\n');
                                        }
                                        br.close();
                                    }
                                    catch (IOException e) {
                                        Utilities.displayDialog(MainActivity.this, "Error Login. Invalid Data.");
                                    }
                                    try {

                                        Account newAccount = Utilities.getAccount(text.toString());
                                        if (newAccount.getPassword().equals(passwordEditText.getText().toString())){
                                            Storage storage = new Storage();
                                            storage.updateEmailValue(MainActivity.this, newAccount.getEmail());
                                            storage.updatePasswordValue(MainActivity.this, newAccount.getPassword());
                                            storage.updateNameValue(MainActivity.this, newAccount.getName());
                                            storage.updateLogoutValue(MainActivity.this, storage.ACTIVE);
                                            progressInd.dismiss();

                                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            Utilities.displayDialog(MainActivity.this, "Invalid Email/Password, Please try again.");
                                        }
                                    } catch (JSONException jexp) {
                                        Utilities.displayDialog(MainActivity.this, "Error Login. Invalid Data.");
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Utilities.displayDialog(MainActivity.this, "Error Login. Invalid Data.");
                                }
                            });
                        } catch (IOException ioExp) {
                            Utilities.displayDialog(MainActivity.this, "Error Login. Invalid Data.");
                        }
                    } else {
                        Utilities.displayDialog(MainActivity.this, "Error login. Invalid Data.");
                    }
                } else {
                    Utilities.displayDialog(MainActivity.this, "Failed to connect to server. Please check your internet connection.");
                }
            }
        });

        /**
         *
         * Go to Create Account page
         */
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, CreateAccountActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        System.exit(0);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Exit Application?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}
