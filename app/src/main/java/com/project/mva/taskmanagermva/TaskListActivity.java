package com.project.mva.taskmanagermva;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.project.mva.taskmanagermva.storage.Storage;
import com.project.mva.taskmanagermva.utils.Task;
import com.project.mva.taskmanagermva.utils.Utilities;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import android.util.Log;

public class TaskListActivity extends Activity {

    private StorageReference mStorageRef;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);

        Storage storage = new Storage();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        if (Utilities.isOnline(TaskListActivity.this)) {
            String email = storage.getEmailValue(TaskListActivity.this);
            StorageReference riversRef = mStorageRef.child("useraccount/"+email+"TASKS"+".txt");
            try {
                final File localFile = File.createTempFile(email+"TASKS","txt");
                riversRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                        StringBuilder text = new StringBuilder();

                        try {
                            BufferedReader br = new BufferedReader(new FileReader(localFile));
                            String line;

                            while ((line = br.readLine()) != null) {
                                text.append(line);
                                text.append('\n');
                            }
                            br.close();
                        }
                        catch (IOException e) {
                            Utilities.displayDialog(TaskListActivity.this, "Failed to retrieve any tasks.");
                        }

                        try {
                            String jData = text.toString();
                            if (jData != null || jData.length() > 0) {
                                Storage storage = new Storage();
                                storage.updateTasksValue(TaskListActivity.this, jData);
                                String[] taskData = Utilities.getTaskTitles(jData);
                                ArrayAdapter adapter = new ArrayAdapter<String>(TaskListActivity.this, R.layout.listitem, taskData);
                                final ListView listview = (ListView) findViewById(R.id.task_list);
                                listview.setAdapter(adapter);
                                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        Object title = listview.getItemAtPosition(position);
                                        Log.d("Title: ",title.toString());
                                        Storage storage = new Storage();
                                        try {
                                            Task task = Utilities.getTask(storage.getTasksValue(TaskListActivity.this), title.toString());
                                            Utilities.displayDialog(TaskListActivity.this,"Task", "Title: "+task.getTitle()+"\n" +
                                                    "Description: "+task.getDescription()+"\n"+"" +
                                                    "Creation Date: "+task.getCreateDate()+"\n");
                                        } catch (JSONException jexp) {
                                            Log.d("JSON exp: ",jexp.getMessage());
                                            Utilities.displayDialog(TaskListActivity.this, "Failed to retrieve any tasks.");
                                        }
                                    }
                                });
                            } else {
                                Utilities.displayDialog(TaskListActivity.this, "Failed to retrieve any tasks.");
                            }
                        } catch (JSONException jexp) {
                            Utilities.displayDialog(TaskListActivity.this, "Failed to retrieve any tasks.");
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Utilities.displayDialog(TaskListActivity.this, "Failed to retrieve any tasks.");
                    }
                });
            } catch (IOException ioExp) {
                Utilities.displayDialog(TaskListActivity.this, "Failed to retrieve any tasks.");
            }
        } else {
            Utilities.displayDialog(TaskListActivity.this, "Failed to connect to server. Please check your internet connection.");
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TaskListActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}

