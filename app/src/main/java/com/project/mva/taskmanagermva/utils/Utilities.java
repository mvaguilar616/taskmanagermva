package com.project.mva.taskmanagermva.utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

/**
 * Created by martinInExt on 16/03/2017.
 */

public class Utilities {

    static AlertDialog alertDialog;

    public static JSONObject createAccount(String email, String password, String name, String status) throws JSONException{
        JSONObject json = new JSONObject();
        JSONObject accountJson = new JSONObject();
        accountJson.put("email", email);
        accountJson.put("password", password);
        accountJson.put("name", name);
        accountJson.put("status", status);
        json.put("account",accountJson);

        return json;
    }

    public static JSONObject updateAccount(String email, String password, String name) throws JSONException{
        JSONObject json = new JSONObject();
        JSONObject accountJson = new JSONObject();
        accountJson.put("email", email);
        accountJson.put("password", password);
        accountJson.put("name", name);
        json.put("account",accountJson);

        return json;
    }

    public static JSONObject createTask(String jData, String title, String description, String createDate) throws JSONException{

        JSONObject jsonTasks = new JSONObject();

        if (jData.length() > 0) {
            JSONObject json = new JSONObject(jData);
            JSONArray jArray = json.getJSONArray("tasks");

            JSONObject jsonTask = new JSONObject();
            jsonTask.put("title", title);
            jsonTask.put("description", description);
            jsonTask.put("createdate", createDate);
            jArray.put(jsonTask);


            jsonTasks.put("tasks", jArray);
        } else {
            JSONObject jsonTask = new JSONObject();
            jsonTask.put("title", title);
            jsonTask.put("description", description);
            jsonTask.put("createdate", createDate);

            JSONArray jArray = new JSONArray();
            jArray.put(jsonTask);

            jsonTasks.put("tasks", jArray);
        }

        return jsonTasks;
    }

    public static Account getAccount(String jData) throws JSONException{

        JSONObject accountJ = new JSONObject(jData).getJSONObject("account");

        Account account = new Account();
        account.setEmail(accountJ.getString("email"));
        account.setPassword(accountJ.getString("password"));
        account.setName(accountJ.getString("name"));

        return account;
    }

    public static Task[] getTasks(String jData) throws JSONException{

        JSONObject jsonObj = new JSONObject(jData);
        JSONArray taskJArray = jsonObj.getJSONArray("tasks");
        int taskLength = taskJArray.length();
        Task[] tasks = new Task[taskLength];

        for (int i = 0; i < taskLength; i++) {
            JSONObject jdata = taskJArray.getJSONObject(i);
            Task task = new Task();
            task.setId(String.valueOf(i));
            task.setTitle(jdata.getString("title"));
            task.setDescription(jdata.getString("description"));
            task.setCreateDate(jdata.getString("createdate"));

            tasks[i] = task;
        }

        return tasks;
    }

    public static Task getTask(String jData, String title) throws JSONException{

        JSONObject jsonObj = new JSONObject(jData);
        JSONArray taskJArray = jsonObj.getJSONArray("tasks");
        int taskLength = taskJArray.length();
        Task choice = new Task();

        for (int i = 0; i < taskLength; i++) {
            JSONObject jdata = taskJArray.getJSONObject(i);
            Task task = new Task();
            task.setTitle(jdata.getString("title"));
            task.setDescription(jdata.getString("description"));
            task.setCreateDate(jdata.getString("createdate"));
            if (task.getTitle().equals(title)) {
                choice = task;
            }
        }

        return choice;
    }

    public static String[] getTaskTitles(String jData) throws JSONException{

        JSONObject jsonObj = new JSONObject(jData);
        JSONArray taskJArray = jsonObj.getJSONArray("tasks");
        int taskLength = taskJArray.length();
        String[] tasksTitles = new String[taskLength];

        for (int i = 0; i < taskLength; i++) {
            JSONObject jdata = taskJArray.getJSONObject(i);
            tasksTitles[i] = jdata.getString("title");
            Log.d("Task Title: ",tasksTitles[i].toString());
        }

        return tasksTitles;
    }

    public static boolean isOnline(Context mContext) {

        ConnectivityManager connMgr = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static void displayDialog(Context mContext, String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setIcon(android.R.drawable.ic_dialog_info);
        builder1.setTitle("Alert!");
        builder1.setMessage(message);
        builder1.setCancelable(true);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        if(alertDialog != null && alertDialog.isShowing()) {
            alertDialog.cancel();
        }

        alertDialog = builder1.create();
        alertDialog.show();
    }

    public static void displayDialog(Context mContext, String title, String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setIcon(android.R.drawable.ic_dialog_info);
        builder1.setTitle(title);
        builder1.setMessage(message);
        builder1.setCancelable(true);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        if(alertDialog != null && alertDialog.isShowing()) {
            alertDialog.cancel();
        }

        alertDialog = builder1.create();
        alertDialog.show();
    }


}
