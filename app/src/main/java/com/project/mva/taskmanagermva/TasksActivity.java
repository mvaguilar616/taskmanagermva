package com.project.mva.taskmanagermva;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import com.project.mva.taskmanagermva.storage.Storage;
import com.project.mva.taskmanagermva.utils.Utilities;

public class TasksActivity extends Activity {

    private StorageReference mStorageRef;
    private Button addTaskButton;
    private EditText titleEditText;
    private EditText descriptionEditText;
    private EditText createDateEditText;
    private TextView welcomeLoginTextView;
    private TextView signinLoginTextView;
    private ProgressDialog progressInd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);

        mStorageRef = FirebaseStorage.getInstance().getReference();
        welcomeLoginTextView = (TextView) findViewById(R.id.welcomeLoginTextView);
        signinLoginTextView = (TextView) findViewById(R.id.signInTextView);
        addTaskButton = (Button) findViewById(R.id.addTaskButton);
        titleEditText = (EditText) findViewById(R.id.titleTaskEditText);
        descriptionEditText = (EditText) findViewById(R.id.descriptionTaskEditText);
        createDateEditText = (EditText) findViewById(R.id.createDateEditText);

        Storage storage = new Storage();

        if (storage.getLogoutValue(TasksActivity.this).equals(Storage.ACTIVE)) {

            String welcome = getString(R.string.welcome);
            String signin = getString(R.string.signin);
            welcomeLoginTextView.setText(welcome +" "+ storage.getNameValue(TasksActivity.this));
            signinLoginTextView.setText(signin +" "+storage.getEmailValue(TasksActivity.this) );

            createDateEditText.setText(DateUtils.formatDateTime(TasksActivity.this, System.currentTimeMillis(),DateUtils.FORMAT_SHOW_DATE));

            /**
             *
             * Create Task using entered Title, Description & Creation Date
             */
            addTaskButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                if (Utilities.isOnline(TasksActivity.this)) {
                    if (titleEditText.getText().length() > 0 & descriptionEditText.getText().length() > 0) {

                        progressInd = new ProgressDialog(v.getContext());
                        progressInd.setCancelable(true);
                        progressInd.setMessage("Creating Task...");
                        progressInd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressInd.setProgress(0);
                        progressInd.setMax(100);
                        progressInd.show();

                        Storage storage = new Storage();
                        String email = storage.getEmailValue(TasksActivity.this);
                        StorageReference riversRef = mStorageRef.child("useraccount/"+email+"TASKS"+".txt");

                        try {
                            final File localFile = File.createTempFile(email+"TASKS","txt");
                            riversRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                                    StringBuilder text = new StringBuilder();

                                    try {
                                        BufferedReader br = new BufferedReader(new FileReader(localFile));
                                        String line;

                                        while ((line = br.readLine()) != null) {
                                            text.append(line);
                                            text.append('\n');
                                        }
                                        br.close();
                                    }
                                    catch (IOException e) {
                                        Utilities.displayDialog(TasksActivity.this, "Error Creation. Invalid Data.");
                                    }

                                    try {
                                        Storage storage = new Storage();
                                        String email = storage.getEmailValue(TasksActivity.this);
                                        StorageReference riversRef = mStorageRef.child("useraccount/"+email+"TASKS"+".txt");
                                        Log.d("JSON DATA FROM SERVER: ", text.toString());
                                        JSONObject data = Utilities.createTask(text.toString(),titleEditText.getText().toString(),
                                                descriptionEditText.getText().toString(),createDateEditText.getText().toString());
                                        String dataAccount = data.toString();
                                        InputStream stream;

                                        stream = new ByteArrayInputStream(dataAccount.getBytes("UTF-8"));
                                        riversRef.putStream(stream).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                titleEditText.setText("");
                                                descriptionEditText.setText("");
                                                Utilities.displayDialog(TasksActivity.this, "Task Created");
                                                progressInd.dismiss();
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Utilities.displayDialog(TasksActivity.this, "Error Creation. Invalid Data.");
                                            }
                                        });
                                    } catch (UnsupportedEncodingException uencExp) {
                                        Utilities.displayDialog(TasksActivity.this, "Error Creation. Invalid Data.");
                                    } catch (JSONException jExp) {
                                        Utilities.displayDialog(TasksActivity.this, "Error Creation. Invalid Data.");
                                    }

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    try {
                                        Storage storage = new Storage();
                                        String email = storage.getEmailValue(TasksActivity.this);
                                        StorageReference riversRef = mStorageRef.child("useraccount/"+email+"TASKS"+".txt");

                                        JSONObject data = Utilities.createTask("",titleEditText.getText().toString(),
                                                descriptionEditText.getText().toString(),createDateEditText.getText().toString());
                                        String dataAccount = data.toString();
                                        InputStream stream;

                                        stream = new ByteArrayInputStream(dataAccount.getBytes("UTF-8"));
                                        riversRef.putStream(stream).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                Utilities.displayDialog(TasksActivity.this, "Task Created");
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {

                                            }
                                        });
                                    } catch (UnsupportedEncodingException uencExp) {
                                        Utilities.displayDialog(TasksActivity.this, "Error Login. Invalid Data.");
                                    } catch (JSONException jExp) {
                                        Utilities.displayDialog(TasksActivity.this, "Error Login. Invalid Data.");
                                    }
                                }
                            });
                        } catch (IOException ioExp) {
                            Utilities.displayDialog(TasksActivity.this, "Error Creation. Invalid Data.");
                        }
                    } else {
                        Utilities.displayDialog(TasksActivity.this, "Error Creation. Invalid Data.");
                    }
                } else {
                    Utilities.displayDialog(TasksActivity.this, "Error Creation. Invalid Data.");
                }
                }
            });
        } else {
            Utilities.displayDialog(TasksActivity.this, "Please Login first at Manager App");
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TasksActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
