package com.project.mva.taskmanagermva.utils;

/**
 * Created by martinInExt on 16/03/2017.
 */

public class Account {

    private String email;
    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() { return this.email; }

    private String password;
    public void setPassword(String password) {
        this.password = password;
    }
    public String getPassword() { return this.password; }

    private String name;
    public void setName(String name) { this.name = name; }
    public String getName() { return this.name; }
}
